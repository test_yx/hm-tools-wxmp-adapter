#### 前言
同微信服务器交互，是以http方式交互，总体为主动请求微信服务器获取反馈数据、被动接收微信服务器发送的数据并处理之后反馈数据至微信服务器。即：
- 主动请求微信服务器
	- get方式
	  - 获取的数据类型为json
	  - 获取的数据类型为 字节流（比如素材管理中的获取图片素材接口）
	- post方式
	  - 请求参数数据类型为json，获取反馈的数据类型为json
	  - 请求参数数据类型为form表单，获取反馈的数据类型为json （比如素材管理中上传图片素材接口）
- 被动接收微信服务器的请求，接收到的数据类型均为xml

#### 概述
本组件包是整个工具包的核心包，主要提供的功能有：
1. 全局配置信息文件管理
2. access token 获取、存储、扩展。
3. 基于动态代理机制（借鉴mybatis）以http请求方式主动请求微信服务器API接口交互数据。
4. 基于反射、路由机制（借鉴spring MVC）解析微信服务器事件消息xml格式文本数据并根据其消息类别选择对应得处理器处理。

#### 基本使用示例
- 以获取自定义菜单为例，[官方接口文档](https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Querying_Custom_Menus.html)，其api接口URI为：`/cgi-bin/get_current_selfmenu_info`
1. 创建对应返回数据的反序列化实体类，具体参照：[ButtonDemo.java](src/test/java/top/hmtools/wxmp/baseDemo/model/ButtonDemo.java)，[MenuDemoBean.java](src/test/java/top/hmtools/wxmp/baseDemo/model/MenuDemoBean.java)，[MenuWapperDemoBean.java](src/test/java/top/hmtools/wxmp/baseDemo/model/MenuWapperDemoBean.java)

2. 创建mapper接口（[MenuMapperDemo.java](src/test/java/top/hmtools/wxmp/baseDemo/MenuMapperDemo.java)）：

```
/**
 * 1、接口必须被 `@WxmpMapper` 注解修饰
 * @author HyboWork
 *
 */
@WxmpMapper
public interface MenuMapperDemo {

	/**
	 * 查询自定义菜单目录
	 * <br>2、方法必须被 `@WxmpApi`注解修饰，其中：httpMethods是指定http请求微信接口的方法（必须的）；uri 是请求微信接口的URI地址（必须的）
	 * <br>3. MenuWapperDemoBean 是用于自动反序列化请求微信接口返回数据为实体类对象实例。
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET, uri = "/cgi-bin/menu/get")
	public MenuWapperDemoBean getMenu();
}

```

3. 获取session（[BaseTest.java](src/test/java/top/hmtools/wxmp/BaseTest.java)）：
```
//1. 初始化全局所使用的配置文件对象实例
WxmpConfiguration configuration = new WxmpConfiguration();
		
//2. 设置 存储appid，appsecret 数据对 的盒子
AppIdSecretBox appIdSecretBox = new AppIdSecretBox() {
	
	@Override
	public AppIdSecretPair getAppIdSecretPair() {
		AppIdSecretPair appIdSecretPair = new AppIdSecretPair();
		appIdSecretPair.setAppid("你的微信公众号appid");
		appIdSecretPair.setAppsecret("你的微信公众号secret");
		return appIdSecretPair;
	}
};

//3. 设置 获取access token 的中间件
DefaultAccessTokenHandle accessTokenHandle = new DefaultAccessTokenHandle(appIdSecretBox);
configuration.setAccessTokenHandle(accessTokenHandle);

//4. 获取session创建工厂
WxmpSessionFactory factory = WxmpSessionFactoryBuilder.build(configuration);

//5. 获取会话
WxmpSession wxmpSession = this.factory.openSession();

```

4. 获取动态代理后的mapper对象实例（[BaseDemo.java](src/test/java/top/hmtools/wxmp/baseDemo/BaseDemo.java)）：

```
//6. 获取被动态代理后的api mapper 对象实例`IMenuApi.class 是使用了 top.hmtools.wxmp.core.annotation.WxmpMapper 注解的接口`
MenuMapperDemo menuMapperDemo = this.wxmpSession.getMapper(MenuMapperDemo.class);

//7. 执行请求，获取反馈结果并打印
MenuWapperDemoBean menu = menuMapperDemo.getMenu();
System.out.println(JSON.toJSONString(menu));

```

打印的反馈数据是：
```
{
    "menu": {
        "button": [
            {
                "name": "哈哈",
                "sub_button": [
                    {
                        "name": "官网",
                        "sub_button": [],
                        "type": "view",
                        "url": "http://m.hybo.net/main/index/index.html"
                    },
                    {
                        "name": "动态",
                        "sub_button": [],
                        "type": "view",
                        "url": "http://hm.hn.cn/main/news/index.html"
                    },
                    {
                        "name": "视频",
                        "sub_button": [],
                        "type": "view",
                        "url": "http://hm.hn.cn/main/news/video.html"
                    }
                ]
            }
        ]
    }
}
```

#### 配置核心类
1. 类全限定路径：top.hmtools.wxmp.core.configuration.WxmpConfiguration
2. 配置项说明：  

属性名称 |  说明  
-|-
charset | 收、发数据时的字符编码，缺省 utf-8
forceAccessTokenString | 强制使用的accessToken字符串，当本属性被设置为不为空，且不为空字符串时，则强制使用该accessToken字符串，以便于使用某些场景
eUrlServer | 微信服务器地址
accessTokenHandle | 访问微信接口时所需的accessToken工具类实例
wxmpSessionFactory | 继承top.hmtools.wxmp.core.WxmpSessionFactory类的对象实例


#### 微信公众号appid、appsecret信息存取盒子
1. 接口全限定路径：`top.hmtools.wxmp.core.configuration.AppIdSecretBox`


#### access token 工具核心类
1. 核心类全限定路径：`top.hmtools.wxmp.core.access_handle.BaseAccessTokenHandle`
2. 缺省的access token本地缓存方案实现类全限定路径：top.hmtools.wxmp.core.access_handle.DefaultAccessTokenHandle
3. 如何获取
```
//1 设置 存储appid，appsecret 数据对 的盒子
AppIdSecretBox appIdSecretBox = new AppIdSecretBox() {
	
	@Override
	public AppIdSecretPair getAppIdSecretPair() {
		AppIdSecretPair appIdSecretPair = new AppIdSecretPair();
		appIdSecretPair.setAppid(AppId.appid);
		appIdSecretPair.setAppsecret(AppId.appsecret);
		return appIdSecretPair;
	}
};

//2 初始化并设置 获取access token 的中间件，只要是继承 top.hmtools.wxmp.core.access_handle.BaseAccessTokenHandle 类即可
DefaultAccessTokenHandle accessTokenHandle = new DefaultAccessTokenHandle(appIdSecretBox);

//3 从微信服务器侧获取access token 并打印
AccessTokenBean accessTokenBean = accessTokenHandle.getAccessTokenBean();
System.out.println(JSON.toJSONString(accessTokenBean));

```
打印的结果是：
```
{
    "access_token": "32_6599V7kLKCSlzEA6GFpKb7AMY_Mg2s5e2z2knVsTMA5qdh2wRAAcfB8TGvrN2YmqEJoZHmnuhPDN0LI-9BbZV_1bNP6kjlyKeHm4da0oEZqA8wrO0nV3Z5P_oMHeHWX8Z6qUckm9N1nJWkD3TLBcAIAEWM",
    "appid": "wxb45a15356607d2e7",
    "expires_in": 7200,
    "lastModifyDatetime": 1588049627924,
    "secret": "5bcc80cb9d5178f1562af313f59c9bb3"
}
```
4. 主要功能：
- `getAccessTokenBean`  获取可用的 access token对象实例，这是个抽象方法，由继承类具体实现，可以是实时获取（不推荐）、获取后本地缓存、向自建的中控服务器获取等方案。
- `getAccessTokenString`  获取可用的 access token 字符串，本方法本质是调用 `getAccessToken`。
- `getAppIdSecretBox` 获取用于存取微信公众号appid、appsecret信息的对象实例。


#### 主动http请求微信服务器
1. 核心类全限定路径：top.hmtools.wxmp.core.DefaultWxmpSession
2. 暂时不提供自定义实现类
3. 本核心类基于httpclient连接池、动态代理（参照mybatis）实现。直接使用`top.hmtools.wxmp.core.DefaultWxmpSession`中诸如`httppost**`、`httpget**`之类的方法是对 httpclient 的再次封装，即直接发送请求到参数中指定的URL地址并获取反馈数据。`getMapper`是很核心的功能方法，通过该方法获取访问微信api接口的动态代理类对象实例（参照mybatis实现），要求其接口被`@WxmpApi`注解修饰类，被`@WxmpApi`注解修饰方法。获取的对象实例，请求微信api接口时均会自动带上 access token参数，使用时只需编写好请求参数、返回结果数据结构类即可。具体实现参见`top.hmtools.wxmp.core.DefaultWxmpSession.invoke(Object, Method, Object[])`。
4. 返回结果基础数据结构类：`top.hmtools.wxmp.core.model.ErrcodeBean`。创建自定义返回结果数据结构时，建议继承该类。

#### 被动解析并处理来自微信服务器的xml数据
1. 基础的消息类全限定路径：top.hmtools.wxmp.core.model.message.BaseMessage
2. 基础的事件通知消息类全限定路径：top.hmtools.wxmp.core.model.message.BaseEventMessage
3. 基础的消息类中已实现将Javabean与xml进行互转，基于 XStream 实现。
4. top.hmtools.wxmp.core.DefaultWxmpMessageHandle 实现了根据xml字符串数据，自动选择对应该消息的处理方法进行处理（参照spring MVC实现）。要求处理类必须被`@WxmpController`注解修饰类，被`@WxmpRequestMapping`注解修饰方法，且方法的形式参数必须是 top.hmtools.wxmp.core.model.message.BaseMessage 子类并被`@WxmpMessage`注解修饰类。具体处理实现参见 `top.hmtools.wxmp.core.DefaultWxmpMessageHandle.addMessageMetaInfo(Object)`与`top.hmtools.wxmp.core.DefaultWxmpMessageHandle.processXmlData(String)`。
5. 使用示例：
```
// 1 创建“路由”实例
DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

// 2 注册xml处理类（类似spring MVC 中的 controller）到“路由”中的容器
WxmpControllerTest wxmpControllerTest = new WxmpControllerTest();
defaultWxmpMessageHandle.addMessageMetaInfo(wxmpControllerTest);

// 3 （可选） 打印容器看看注册情况
System.out.println(JSON.toJSONString(this.defaultWxmpMessageHandle.getEventMap()));
System.out.println(JSON.toJSONString(this.defaultWxmpMessageHandle.getMsgTypeMap()));

// 4 解析并处理微信事件通知消息xml字符串
String xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[fromUser]]></FromUserName><CreateTime>1348831860</CreateTime><MsgType><![CDATA[text]]></MsgType><Content><![CDATA[this is a test]]></Content><MsgId>1234567890123456</MsgId></xml>";
Object processXmlData = this.defaultWxmpMessageHandle.processXmlData(xml);
System.out.println(processXmlData);
```





















