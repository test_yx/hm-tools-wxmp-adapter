package top.hmtools.wxmp.core.enums;

public enum HttpParamDataType {

	/**
	 * 文本
	 */
	TEXT,
	
	/**
	 * 文件
	 */
	FILE,
	
	/**
	 * 输入流
	 */
	INPUTSTREAM,
	
	/**
	 * 实体类
	 */
	JAVABEAN,
	
	/**
	 * 字节数组
	 */
	BYTE_ARRAY;
}
