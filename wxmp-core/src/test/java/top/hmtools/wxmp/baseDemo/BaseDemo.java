package top.hmtools.wxmp.baseDemo;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.baseDemo.model.MenuWapperDemoBean;

public class BaseDemo extends BaseTest {

	@Test
	public void testGetMenu(){
		MenuMapperDemo menuMapperDemo = this.wxmpSession.getMapper(MenuMapperDemo.class);
		MenuWapperDemoBean menu = menuMapperDemo.getMenu();
		System.out.println(JSON.toJSONString(menu));
	}
}
