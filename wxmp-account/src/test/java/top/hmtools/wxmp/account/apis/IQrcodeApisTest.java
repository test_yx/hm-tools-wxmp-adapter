package top.hmtools.wxmp.account.apis;

import java.io.InputStream;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.account.enums.EQRActionName;
import top.hmtools.wxmp.account.models.ActionInfo;
import top.hmtools.wxmp.account.models.QRCodeParam;
import top.hmtools.wxmp.account.models.QRCodeResult;
import top.hmtools.wxmp.account.models.Scene;
import top.hmtools.wxmp.account.models.TicketParam;
import top.hmtools.wxmp.core.WxmpSession;

public class IQrcodeApisTest extends BaseTest {
	
	protected WxmpSession wxmpSession;
	private IQrcodeApis qrcodeApis ;
	

	@Test
	public void testCreate() {
		QRCodeParam codeParam = new QRCodeParam();
		
		ActionInfo action_info = new ActionInfo();
		Scene scene = new Scene();
		scene.setScene_str("aaaabbbbcccc");
		action_info.setScene(scene);
		codeParam.setAction_info(action_info);
		
		codeParam.setAction_name(EQRActionName.QR_STR_SCENE);
		
		codeParam.setExpire_seconds(20*60*60*60L);
		QRCodeResult create = this.qrcodeApis.create(codeParam);
		System.out.println(create);
	}

	@Test
	public void testShowQrCode() {
		TicketParam ticketParam = new TicketParam();
		ticketParam.setTicket("gQEy8DwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAyODQtVVlPcG5kQ2oxd09SMmh0Y3MAAgQyqFpdAwQAjScA");
		InputStream showQrCode = this.qrcodeApis.showQrCode(ticketParam);
		System.out.println(showQrCode);
	}

}
