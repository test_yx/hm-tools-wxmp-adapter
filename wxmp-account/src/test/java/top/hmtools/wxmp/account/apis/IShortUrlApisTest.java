package top.hmtools.wxmp.account.apis;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.account.models.ShorturlParam;
import top.hmtools.wxmp.account.models.ShorturlResult;
import top.hmtools.wxmp.core.WxmpSession;

public class IShortUrlApisTest extends BaseTest{
	
	protected WxmpSession wxmpSession;
	private IShortUrlApis shortUrlApis ;
	

	@Test
	public void testGetShorUrl() {
		ShorturlParam shorturlParam = new ShorturlParam();
		shorturlParam.setAction("long2short");
		shorturlParam.setLong_url("https://3w.huanqiu.com/a/3458fa/7PhIUEtAEzS?agt=8");
		ShorturlResult shorUrl = this.shortUrlApis.getShorUrl(shorturlParam);
		System.out.println(shorUrl);
	}

}
