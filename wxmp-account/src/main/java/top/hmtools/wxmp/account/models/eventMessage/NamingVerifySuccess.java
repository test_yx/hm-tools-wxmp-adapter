package top.hmtools.wxmp.account.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.model.message.BaseEventMessage;

/**
 * 名称认证成功（即命名成功）
 * <br>
 * {@code 
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>  
  <FromUserName><![CDATA[fromUser]]></FromUserName>  
  <CreateTime>1442401093</CreateTime>  
  <MsgType><![CDATA[event]]></MsgType>  
  <Event><![CDATA[naming_verify_success]]></Event>  
  <ExpiredTime>1442401093</ExpiredTime> 
</xml>
 * }
 * @author HyboWork
 *
 */
public class NamingVerifySuccess extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2322182837702404558L;
	
	@XStreamAlias("ExpiredTime")
	private Long expiredTime;
	
	

	public Long getExpiredTime() {
		return expiredTime;
	}



	public void setExpiredTime(Long expiredTime) {
		this.expiredTime = expiredTime;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	@Override
	public String toString() {
		return "NamingVerifySuccess [expiredTime=" + expiredTime + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
