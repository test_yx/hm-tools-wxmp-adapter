package top.hmtools.wxmp.message.group.model.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ItemType {

	@XStreamAlias(value = "ArticleIdx")
	protected String articleIdx;
	@XStreamAlias(value = "UserDeclareState")
	protected String userDeclareState;
	@XStreamAlias(value = "AuditState")
	protected String auditState;
	@XStreamAlias(value = "OriginalArticleUrl")
	protected String originalArticleUrl;
	@XStreamAlias(value = "OriginalArticleType")
	protected String originalArticleType;
	@XStreamAlias(value = "CanReprint")
	protected String canReprint;
	@XStreamAlias(value = "NeedReplaceContent")
	protected String needReplaceContent;
	@XStreamAlias(value = "NeedShowReprintSource")
	protected String needShowReprintSource;

	public String getArticleIdx() {
		return articleIdx;
	}

	public void setArticleIdx(String articleIdx) {
		this.articleIdx = articleIdx;
	}

	public String getUserDeclareState() {
		return userDeclareState;
	}

	public void setUserDeclareState(String userDeclareState) {
		this.userDeclareState = userDeclareState;
	}

	public String getAuditState() {
		return auditState;
	}

	public void setAuditState(String auditState) {
		this.auditState = auditState;
	}

	public String getOriginalArticleUrl() {
		return originalArticleUrl;
	}

	public void setOriginalArticleUrl(String originalArticleUrl) {
		this.originalArticleUrl = originalArticleUrl;
	}

	public String getOriginalArticleType() {
		return originalArticleType;
	}

	public void setOriginalArticleType(String originalArticleType) {
		this.originalArticleType = originalArticleType;
	}

	public String getCanReprint() {
		return canReprint;
	}

	public void setCanReprint(String canReprint) {
		this.canReprint = canReprint;
	}

	public String getNeedReplaceContent() {
		return needReplaceContent;
	}

	public void setNeedReplaceContent(String needReplaceContent) {
		this.needReplaceContent = needReplaceContent;
	}

	public String getNeedShowReprintSource() {
		return needShowReprintSource;
	}

	public void setNeedShowReprintSource(String needShowReprintSource) {
		this.needShowReprintSource = needShowReprintSource;
	}

	@Override
	public String toString() {
		return "ItemType [articleIdx=" + articleIdx + ", userDeclareState=" + userDeclareState + ", auditState="
				+ auditState + ", originalArticleUrl=" + originalArticleUrl + ", originalArticleType="
				+ originalArticleType + ", canReprint=" + canReprint + ", needReplaceContent=" + needReplaceContent
				+ ", needShowReprintSource=" + needShowReprintSource + "]";
	}

}
