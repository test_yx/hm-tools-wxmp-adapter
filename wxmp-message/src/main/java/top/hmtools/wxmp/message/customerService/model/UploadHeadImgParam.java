package top.hmtools.wxmp.message.customerService.model;

import java.io.File;

public class UploadHeadImgParam {

	private String kf_account;
	
	private File image;

	public String getKf_account() {
		return kf_account;
	}

	public void setKf_account(String kf_account) {
		this.kf_account = kf_account;
	}

	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}
	
	
}
