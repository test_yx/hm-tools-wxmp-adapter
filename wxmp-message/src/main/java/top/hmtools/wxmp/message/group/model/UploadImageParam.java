package top.hmtools.wxmp.message.group.model;

import java.io.File;

public class UploadImageParam {

	private File media;

	public File getMedia() {
		return media;
	}

	public void setMedia(File media) {
		this.media = media;
	}

}
