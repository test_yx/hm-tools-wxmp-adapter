package top.hmtools.wxmp.message.group.model.openIdGroupSend;

/**
 * Auto-generated: 2019-08-26 11:16:6
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Wxcard {

	private String card_id;

	public void setCard_id(String card_id) {
		this.card_id = card_id;
	}

	public String getCard_id() {
		return card_id;
	}

}