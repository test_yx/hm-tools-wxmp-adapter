package top.hmtools.wxmp.message.group.model.preview;

/**
 * 文本
 * @author HyboWork
 *
 */
public class TextPreviewParam extends BasePreviewParam {

	private Text text;
	
	public Text getText() {
		return text;
	}
	
	public void setText(Text text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "TextPreviewParam [text=" + text + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
}
