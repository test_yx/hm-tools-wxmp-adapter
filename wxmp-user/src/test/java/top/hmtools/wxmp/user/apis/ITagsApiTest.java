package top.hmtools.wxmp.user.apis;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import top.hmtools.wxmp.BaseTest;
import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.user.model.BatchTagParam;
import top.hmtools.wxmp.user.model.TagFunsParam;
import top.hmtools.wxmp.user.model.TagFunsResult;
import top.hmtools.wxmp.user.model.TagListParam;
import top.hmtools.wxmp.user.model.TagListResult;
import top.hmtools.wxmp.user.model.TagParam;
import top.hmtools.wxmp.user.model.TagWapperParam;
import top.hmtools.wxmp.user.model.TagWapperResult;
import top.hmtools.wxmp.user.model.TagsWapperResult;

public class ITagsApiTest extends BaseTest {
	
	protected WxmpSession wxmpSession;
	private ITagsApi tagsApi ;
	

	@Test
	public void testCreate() {
		TagWapperParam param = new TagWapperParam();
		TagParam tag = new TagParam();
		tag.setName("人才");
		param.setTag(tag);
		TagWapperResult create = this.tagsApi.create(param);
		System.out.println(create);
	}

	@Test
	public void testGet() {
		TagsWapperResult tagsWapperResult = this.tagsApi.get();
		System.out.println(tagsWapperResult);
	}

	@Test
	public void testUpdate() {
		TagWapperParam param = new TagWapperParam();
		TagParam tag = new TagParam();
		tag.setId(104);
		tag.setName("super人才");
		param.setTag(tag);
		ErrcodeBean update = this.tagsApi.update(param);
		System.out.println(update);
	}

	@Test
	public void testDelete() {
		TagWapperParam param = new TagWapperParam();
		TagParam tag = new TagParam();
		tag.setId(104);
		param.setTag(tag);
		ErrcodeBean delete = this.tagsApi.delete(param);
		System.out.println(delete);
	}

	@Test
	public void testGetFunsOfTag() {
		TagFunsParam param = new TagFunsParam();
		param.setTagid(100);
		TagFunsResult funsOfTag = this.tagsApi.getFunsOfTag(param);
		System.out.println(funsOfTag);
	}

	@Test
	public void testBatchTagging() {
		BatchTagParam batchTagParam = new BatchTagParam();
		batchTagParam.setTagid(100);
		List<String> aa = new ArrayList<String>();
		aa.add("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		batchTagParam.setOpenid_list(aa);
		ErrcodeBean batchTagging = this.tagsApi.batchTagging(batchTagParam);
		System.out.println(batchTagging);
	}

	@Test
	public void testBatchUntagging() {
		BatchTagParam batchTagParam = new BatchTagParam();
		batchTagParam.setTagid(100);
		List<String> aa = new ArrayList<String>();
		aa.add("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		batchTagParam.setOpenid_list(aa);
		ErrcodeBean batchUntagging = this.tagsApi.batchUntagging(batchTagParam);
		System.out.println(batchUntagging);
	}

	@Test
	public void testGetTaglistByOpenid() {
		TagListParam param = new TagListParam();
		param.setOpenid("o2ddm028EcvP1GZGBZG_chnnpc1Y");
		TagListResult taglistByOpenid = this.tagsApi.getTaglistByOpenid(param);
		System.out.println(taglistByOpenid);
	}

}
